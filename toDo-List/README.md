
# ToDo List App

Como requisitado, colocarei neste MarkDown minhas observações quanto ao teste
## Quanto ao teste em si
Achei ele normal. Nada facil ou extremamente dificil e sim um meio termo. 
### Primeiro parte:
Precisava decidir qual caminho seguiria, qual biblioteca Ui usaria, banco de dados e etc...
Como no email que recebi, não havia uma restrição propriamente dita sobre qual banco usar, optei pelo Firebase, pois o mesmo é gratuito e conta com o realtime, algo que o DynamoDb da Amazon não consegue fazer, que foi uma das opções da qual eu ponderei.
Logo após, escolhi usar a biblioteca Nebular, pois ela esta em bastant crescimento seu dashboard, o ngx-admin foi um dos mais baixados no ano de 2018 segundo o github. Instalei tambem o PrimeNG, caso precisa-se de algo mais robusto
Depois disso, escolher o layout. A principio escolhi apenas fazer o simples, mas como ja estava avançando bem, decidi copiar o design do planner do office 365. E assim terminei a primeiro parte com:
* Banco de Dados: Firebase
* Biblioteca Ui: Nebular
* Projeto em: 20%
### Segundo parte:
Vi que iria levar tempo demais para implementar todas as melhorias que eu tinha em mente, mas tentei elas mesmo assim. Passei dois dias tentando implementar novos jeitos de unir um toDo App a um modelo do Planner. Não foi dificil, mas ficou faltando muitas coisas e varias outras ficaram por fazer, o que eu então decidi cortar no modelo final, ficando apenas com um toDoApp.
Depois de construir toda a interface, criei os serviços para gerir o login e a gravação de dados.
E assim terminei a segunda parte:
* Projeto em: 40%
* Subindo para o S3 da AWS
* Criando serviços
### Terceira parte
Terminei de montar a parte visual, mas conforme a data final chegou, não consegui implementar totalmente o tratamento de erros, deixando apenas a parte de login com esse tratamento. Ajustei algumas coisas para deixar o app mais leve, e limpei todos os console logs, para poder entrega-lo o mais limpo possivel.
