export interface UserInterface {
  NameDisplay: string;
  ImageDisplay: string;
  Token: string;
}
