import { Component, OnInit } from '@angular/core';
import { UserInterface } from 'src/app/models/user.interface';
import { AngularFireAuth } from '@angular/fire/auth';
import { LoginService } from 'src/app/service/login.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  usuario: UserInterface = {
    NameDisplay: null,
    ImageDisplay: null,
    Token: null,
  }
  isLogged = false;
  constructor(public auth: AngularFireAuth, public loginSrv: LoginService) {
    this.auth.user.subscribe((user) => {
      console.log('usuario', user);
    })

    this.auth.authState.subscribe((user) => {
      console.log(user);
      if (user !== null) {
        this.loginSrv.usuario.NameDisplay = user.displayName;
        this.loginSrv.usuario.ImageDisplay = user.photoURL;
        this.loginSrv.usuario.Token = user.uid;
      } else {
        this.loginSrv.usuario.NameDisplay = null;
        this.loginSrv.usuario.ImageDisplay = null;
        this.loginSrv.usuario.Token = null;
      }
    });
  }

  ngOnInit() {
    if (this.usuario.Token === '' || this.usuario.Token === undefined || this.usuario.Token === null) {
      this.isLogged = false;
    } else {
      this.isLogged = true;
    }
  }

  logIn() {
    this.loginSrv.Login();
  }
  logOut() {
    this.loginSrv.Logout();
  }

}
