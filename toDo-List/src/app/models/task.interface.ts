export interface TaskInterface {
  titulo: string;
  status: 'Emergencial' | 'Não Importante' | 'Importante' | 'criada' | 'Anotações';
  concluido: boolean;
  criadaEm: string;
  finalizadaEm: string;
  excluir: boolean;
}
