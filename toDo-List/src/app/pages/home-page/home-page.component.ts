import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { LoginService } from './../../service/login.service';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { DataService } from 'src/app/service/data.service';
import { TaskInterface } from './../../models/task.interface';
import { AngularFireDatabase } from '@angular/fire/database';
import { NbToastrService } from '@nebular/theme';
import * as lodash from 'lodash';
@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  DataItem: TaskInterface = {
    titulo: null,
    status: null,
    concluido: null,
    criadaEm: null,
    finalizadaEm: null,
    excluir: null
  };
  inputTest = 'Importante';
  DataList: TaskInterface[] = []
  @ViewChild('listDrag') listDrag: ElementRef;
  constructor(
    public loginSrv: LoginService,
    public dataSrv: DataService,
    private db: AngularFireDatabase,
    private toastrService: NbToastrService) {

    const dia = new Date().getDate()
    const mes = Number(new Date().getMonth()) + 1;
    const ano = new Date().getFullYear()
    const dataAtual = dia + '/' + mes + '/' + ano;
    this.DataItem.criadaEm = dataAtual;
    this.db.list(this.dataSrv.Envio).valueChanges().subscribe((res: TaskInterface[]) => {
      this.DataList = [];
      this.DataList = res;
    });
  }

  ngOnInit() {
  }


  sliptTasks(): TaskInterface[] {
    const unique = lodash.uniqBy(this.DataList, 'status');
    return unique;
  }
  drop(event: CdkDragDrop<TaskInterface[]>) {
    moveItemInArray(this.DataList, event.previousIndex, event.currentIndex);
  }


  random() {
    const random = ['Emergencial', 'Não Importante', 'Importante', 'criada', 'Anotações']
    return lodash.sample(random);
  }

  concluido(event, item: TaskInterface) {

    this.dataSrv.writeData(this.DataList).then((res) => {
      if (item.concluido) {
        this.toastrService.info('Tarefa Conluida');
      }
    });

  }
  addItem() {

    this.DataList.push({
      titulo: this.DataItem.titulo,
      status: this.random(),
      concluido: false,
      criadaEm: this.DataItem.criadaEm,
      finalizadaEm: null,
      excluir: false
    });
    if (this.loginSrv.usuario.Token !== null) {
      this.dataSrv.writeData(this.DataList).then((res) => {
        this.toastrService.success('Tarefa Incluida');
      });
    } else {
      this.toastrService.success('Para salvar suas tarefas na nuvem, efetue o Login', 'Tarefa Incluida');
    }

  }
  deleteItem(item: TaskInterface) {
    item.excluir = true;
    lodash.remove(this.DataList, ((del: TaskInterface) => {
      return del.excluir === true;
    }));
    this.dataSrv.writeData(this.DataList).then((res) => {
      this.toastrService.danger('', 'Tarefa Excluida');
    });
  }

}
