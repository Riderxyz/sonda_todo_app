import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable()
export class DataService {
  public Envio: string;
  private userId = sessionStorage.getItem('UserToken');
  private UserName = sessionStorage.getItem('Username');
  constructor(public db: AngularFireDatabase) {
    this.Envio = 'Usuario/' + this.UserName + ' - ' + this.userId;
  }
  getDataLista() {
    const retorno = [];
    const promise = new Promise((resolve, reject) => {
      this.db.list(this.Envio).valueChanges().subscribe((s) => {
        s.forEach(element => {
          retorno.push(element);
        });
        resolve(retorno);
      });
    });
    return promise;
  }


  writeData(data) {
    const promise = new Promise((resolve, reject) => {
      this.db.object(this.Envio).set(
        data
      ).then((s) => {
        resolve(data);
      });
    });
    return promise;
  }

}
