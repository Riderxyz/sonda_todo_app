import { Component, OnInit, Input } from '@angular/core';
import { TaskInterface } from 'src/app/models/task.interface';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-task-lists',
  templateUrl: './task-lists.component.html',
  styleUrls: ['./task-lists.component.scss']
})
export class TaskListsComponent implements OnInit {
  @Input() listName: string;

  DataItem: TaskInterface = {
    titulo: null,
    status: null,
    concluido: null,
    criadaEm: null,
    finalizadaEm: null,
    excluir: null
  };
  DataList: TaskInterface[] = [];
  taskListType = {
    cor: null as string,
    nome: null as string,
    id: null as string
  }
  constructor(public dataSrv: DataService) {
  }

  ngOnInit() {
    console.log(this.listName);
    switch (this.listName) {
      case 'Importante':
        this.taskListType.cor = 'warning';
        this.taskListType.nome = 'Tarefas Importantes';
        this.taskListType.id = this.listName;
        break;
      case 'Emergencial':
        this.taskListType.cor = 'danger';
        this.taskListType.nome = 'Tarefas Urgentes';
        this.taskListType.id = this.listName;
        break;
      case 'Não Importante':
        this.taskListType.cor = 'info';
        this.taskListType.nome = 'Tarefas Não Importantes';
        this.taskListType.id = this.listName;
        break;
      case 'Anotações':
        this.taskListType.cor = 'primary';
        this.taskListType.nome = 'Anotações';
        this.taskListType.id = this.listName;
        break;
      default:
        break;
    }
  }


  addItem() {
    this.DataList.push({
      titulo: this.DataItem.titulo,
      status: 'Importante',
      concluido: false,
      criadaEm: this.DataItem.criadaEm,
      finalizadaEm: null,
      excluir: false
    });
    this.dataSrv.writeData(this.DataList).then((res) => {
      console.log(res);
    });
  }

}
