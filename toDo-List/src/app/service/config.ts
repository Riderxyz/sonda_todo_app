export const config = {
  errorCodes: {
    fecharPopUp: {
      codigo: 'auth/popup-closed-by-user',
      mensagemPrincipal: 'PopUp ou Janela de Login foram fechados',
      mensagemSecundaria: 'A janela ou o pop-up usado para efetuar o login foram fechados antes de concluir o mesmo',
    },
    tokenExpirado: {
      codigo: 'auth/id-token-expired',
      mensagemPrincipal: 'Token Expirado',
      mensagemSecundaria: 'Efetue o login novamente para renova-lo',
    },
    tokenRevogado: {
      codigo: 'auth/id-token-revoked',
      mensagemPrincipal: 'Token Revogado',
      mensagemSecundaria: 'Efetue o login novamente para renova-lo ou contate o adminstrador caso esse problema persista',
    },
    argumentoInvalido: {
      codigo: 'auth/invalid-argument',
      mensagemPrincipal: 'Itens informados invalidos',
      mensagemSecundaria: 'o usuario ou a senha que você informou são invalidos. Por favor, tente novamente',

    }

  }
}

