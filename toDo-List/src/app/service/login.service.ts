import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';
import { UserInterface } from '../models/user.interface';
import { NbToastrService } from '@nebular/theme';
import { config } from './config';

@Injectable()
export class LoginService {
  usuario: UserInterface = {
    NameDisplay: null,
    ImageDisplay: null,
    Token: null,
  }
  constructor(public afAuth: AngularFireAuth, private toastrService: NbToastrService) { }


  Login() {
    firebase.auth().languageCode = 'pt';
    this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider().setCustomParameters({
      prompt: 'select_account'
    })).then((res) => {
      this.toastrService.success('Usuario Logado', 'Seja bem vindo(a)' + res.user.displayName);
    }).catch((err) => {
      console.log('O erro', err);
      this.isInError(err);
    });
  }

  Logout() {
    this.afAuth.auth.signOut().then((res) => {
      console.log(res);
    });
  }

  isInError(err) {
    switch (err.code) {
      case config.errorCodes.fecharPopUp.codigo:
        this.toastrService.danger(config.errorCodes.fecharPopUp.mensagemSecundaria, config.errorCodes.fecharPopUp.mensagemPrincipal, { duration: 5000 });
        break;
      case config.errorCodes.tokenExpirado.codigo:
        this.toastrService.danger(config.errorCodes.tokenExpirado.mensagemSecundaria, config.errorCodes.tokenExpirado.mensagemPrincipal, { duration: 5000 });
        break;
      case config.errorCodes.tokenRevogado.codigo:
        this.toastrService.danger(config.errorCodes.tokenRevogado.mensagemSecundaria, config.errorCodes.tokenRevogado.mensagemPrincipal, { duration: 5000 });
        break;
      case config.errorCodes.argumentoInvalido.codigo:
        this.toastrService.danger(config.errorCodes.argumentoInvalido.mensagemSecundaria, config.errorCodes.argumentoInvalido.mensagemPrincipal, { duration: 5000 });
        break;

      default:
        break;
    }
  }
}
